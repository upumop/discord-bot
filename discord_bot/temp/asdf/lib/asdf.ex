defmodule Asdf do
  @moduledoc """
  Documentation for `Asdf`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Asdf.hello()
      :world

  """
  def hello do
    :world
  end
end
