
defmodule DiscordBot.Consumer do
  use Nostrum.Consumer

  alias Nostrum.Api

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:MESSAGE_CREATE, msg, _ws_state} = full) do
    case msg.content do
      "!about" ->
        Api.create_message(msg.channel_id, "I'm a community run bot! See and contribute to my code at https://gitlab.com/upumop/discord-bot.")

      "!infra" ->
        Api.create_message(msg.channel_id, "With each commit to `master`, I'm containerized and deployed to AWS using GitLab CICD.")

      "!ping" ->
        Api.create_message(msg.channel_id, "quack")

      "!raise" ->
        # This won't crash the entire Consumer.
        raise "No problems here!"

      "!sleep" ->
        Api.create_message(msg.channel_id, "Going to sleep...")
        # This won't stop other events from being handled.
        Process.sleep(3000)

      _ ->
        :ignore
    end
  end

  # Default event handler, if you don't include this, your consumer WILL crash if
  # you don't have a method definition for each event type.
  def handle_event(_event) do
    :noop
  end
end
