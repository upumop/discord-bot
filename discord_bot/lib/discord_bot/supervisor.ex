defmodule DiscordBot.Application do
  use Application
  alias DiscordBot.Consumer

  @impl true
  def start(_type, _args) do
    children = [
      Consumer
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DiscordBot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
