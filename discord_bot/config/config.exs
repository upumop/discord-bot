import Config

config :nostrum,
  token: System.get_env("SD_TOKEN"), # The token of your bot as a string
  num_shards: :auto # The number of shards you want to run your bot under, or :auto.

config :porcelain,
  driver: Porcelain.Driver.Basic
