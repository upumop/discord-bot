FROM elixir:alpine

ARG SD_TOKEN

COPY . .

WORKDIR /discord_bot

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get 
RUN MIX_ENV=prod mix release

ENTRYPOINT _build/prod/rel/discord_bot/bin/discord_bot start