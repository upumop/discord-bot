# discord-bot

## Community bot

Feel free to tinker with this bot as you see fit! ([Here](./discord_bot/lib/discord_bot/consumer.ex) is a good place to start!)

## Resources

- Elixir language
  - <https://elixir-lang.org/>
- Bot library documentation
  - <https://github.com/Kraigie/nostrum>

- Run options
  - Biuld and run Elixir build as a service on the same machine
    - <https://docs.gitlab.com/runner/install/>
    - <https://dev.to/seojeek/phoenix-deploys-with-elixir-1-9-with-systemd-no-docker-1od0>
  - Build it in a container
